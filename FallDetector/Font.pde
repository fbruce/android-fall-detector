class Font
{
  PFont defaultFont;
  int size = 0;
  Font()//creates font using Arial
  {
    size = displayWidth/20;
    defaultFont = createFont("Arial-BoldMT-48", size, true);
  }
  PFont getFont()
  {
    return defaultFont;
  }
  void setFont(PFont newFont)
  {
    defaultFont = newFont;
  }
  int getSize()
  {
    return size;
  }
  void setSize(int size)
  {
    this.size = size;
  }
}
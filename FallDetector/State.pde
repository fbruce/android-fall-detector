interface State//interface for all states to inherit fromt 
{
  void draw();
  void update();
  void init();
  void clearScreen();
}
class FallDetectState implements State
{
  Button backButton, cancelFallButton;
  int timeOnScreen;
  boolean fallCancelled = false, cancelButtonDraw = false;
  boolean emailSent = false;

  FallDetectState()
  {
    backButton = new Button("Go Back", 3.0);
    cancelFallButton = new Button("Cancel Fall", 1.0);
  }
  void init()
  {
    backButton.init();
    cancelFallButton.init();
  }
  void checkFall()
  {
    if (validFallDetected == true)
      cancelButtonDraw = true;
  }
  void draw()
  {
    clearScreen();
    backButton.draw();
    String number = String.valueOf(linY);
    onScreenText.draw("Currently Detecting Falls", standardWidth, 0 + (screenOffset * 2), defaultFont.getSize(), defaultFont.getFont());
    onScreenText.draw(number, standardWidth, 0 + (screenOffset * 3), defaultFont.getSize(), defaultFont.getFont());
    //if a fall has happened
    if (cancelButtonDraw)
    {//if time is less than set max time
      if (timeOnScreen < fallTimeInt*30)
      {//draw button, play alarm, count time on screen
        onScreenText.draw("Cancel Fall If Not Valid", standardWidth, 0 + (screenOffset * 4), defaultFont.getSize(), defaultFont.getFont());
        cancelFallButton.draw();
        timeOnScreen++; 
        alarmMusic.play();
      } else //when it equals the max time
      if (timeOnScreen == fallTimeInt*30)
      {//change the text, loop the alarm, send email
        onScreenText.draw("Third Party Contacted", standardWidth, 0 + (screenOffset * 4), defaultFont.getSize(), defaultFont.getFont());
        alarmMusic.play();
        emailSent = true;
        if (emailSent)//means email is only sent once
        {
          runSendEmailChoreo();
          emailSent = false;
          timeOnScreen = 0;//reset time and button drawn boolean
          cancelButtonDraw = false;
        }
      }       
      else
      {
      validFallDetected = false;
      alarmMusic.stop();
      }
    }
    stroke(0);
  }
  void update()
  {
    if (backButton.checkMouse() && mousePressed)
    {
      currentState = 0;
    }
    if (cancelFallButton.checkMouse() && mousePressed)
    {
      cancelButtonDraw = false;
      timeOnScreen = 0;
    }
  }
  void clearScreen()
  {
    background(appColour[0], appColour[1], appColour[2]);
  }
  void changeBackgroundColour()
  {
    background(255, 0, 0);
  }
}
class Button
{
  int x, y, Xsize, Ysize, textOffsetX, textOffsetY;
  String text;
  int offset;
  float numButton;
  int size, sizeOffset;
  
  //sets text and position of button based on what number it is set to
  Button(String text, float numButton)
  {
    this.text = text;
    this.numButton = numButton;
  }
  void init()
  {
    //sets a variety of sizes and placements based on the device width and height
    //this allows for resizing when device changes
    Xsize = displayWidth /2;
    Ysize = displayHeight/ 10;
    offset = displayWidth/4;
    offset =  offset * (int)numButton;
    x = displayWidth/2 - Xsize/2;
    y = displayHeight/2 + offset - Ysize;
    textOffsetX = Xsize/2;
    textOffsetY = Ysize/2;
    size = displayWidth/20;
    sizeOffset = size/10;
  }
  void draw()
  {
    //draws rectangles for buttons and places text on top using text class
    init();
    rectMode(CORNER);
    fill(0, 0, 0, 150);
    strokeWeight(2);
    rect(x, y, Xsize, Ysize, 18);
    fill(255, 255, 255, 255);
    onScreenText.draw(text,x+textOffsetX, y+textOffsetY, size, defaultFont.getFont());
  }
  boolean checkMouse()
  {
    if (mouseX< x + Xsize && mouseX > x && mouseY < y  + Ysize && mouseY > y)
    {
      return true;
    }   
    return false;
  }
  void changeText(String newText)
  {
    //allows for changing of text on buttons after creation
    this.text = newText;
  }
  void hide()
  {
    //hides button offscreen
    rectMode(CORNER);
    fill(255, 255, 255, 150);
    strokeWeight(2);
    rect(x+1000, y+1000, Xsize, Ysize, 18);
    fill(0, 0, 0, 255);
    textSize(30);
    text(text, x + textOffsetX, y + textOffsetY);
  }
}
void runSendEmailChoreo() 
{
  // Create the Choreo object using your Temboo session
  SendEmail sendEmailChoreo = new SendEmail(session);

  // Set inputs - uses name and email from setup
  sendEmailChoreo.setMessageBody(nameInput + " has fallen, please get help!");
  sendEmailChoreo.setSubject("Fall Detected - Please Attend");
  sendEmailChoreo.setPassword("pjrktrlnklwseihn");
  sendEmailChoreo.setUsername("falldetectorUWS@gmail.com");
  sendEmailChoreo.setToAddress(emailInput);//uses set email address

  // Run the Choreo and store the results
  SendEmailResultSet sendEmailResults = sendEmailChoreo.run();

  // Print results
  print(sendEmailResults.getSuccess());
}
class SetupState implements State
{
  Button backButton, enterButton;
  int screenCount;
  boolean initialScreen = true;
  boolean nameScreen = false;
  boolean fallTimeScreen = false;
  boolean emailPart2Screen = false;
  boolean emailPart1Screen = false;
  boolean emailPart3Screen = false;
  Text onScreenText;
  SetupState()
  {
    backButton = new Button("Go Back", 2.0);
    enterButton = new Button ("Enter Details", 1.0);
    screenCount = 0;
    onScreenText = new Text();
  }
  void init()
  {
    backButton.init();
    enterButton.init();
    //loads data, it is saved in an array, it is made into a single string then email is appended to one whole email address and fall time is converted to an int
    loadData();
    makeDataSingleString();
    appendStrings(emailFirstInput, emailSecondInput, emailThirdInput);
    fallTimeInt = setupState.stringToInt(fallTimeInput);
  }
  void draw()
  {
    clearScreen();
    backButton.draw();
    enterButton.draw();
    fill(255, 255, 255, 255);
    textAlign(CENTER);
    switch(screenCount)
    {//draw instructions dependent on input stage
    case 0:
      onScreenText.draw("Enter Name", standardWidth, 0 + screenOffset, defaultFont.getSize(), defaultFont.getFont());
      onScreenText.draw("Time in seconds until email is sent", standardWidth, 0 + screenOffset*2, defaultFont.getSize(), defaultFont.getFont());
      onScreenText.draw("And email address to send to", standardWidth, 0 + screenOffset*3, defaultFont.getSize(), defaultFont.getFont());
      onScreenText.draw("Press Button when ready to enter", standardWidth, 0 + screenOffset*4, defaultFont.getSize(), defaultFont.getFont());
      break;
    case 1:
      onScreenText.draw("Enter Name on Keyboard", standardWidth, 0 + screenOffset, defaultFont.getSize(), defaultFont.getFont());
      onScreenText.draw(nameInput, standardWidth, 0 + screenOffset *2, defaultFont.getSize(), defaultFont.getFont());
      enterButton.changeText("Name Correct");
      break;
    case 2:
      onScreenText.draw("Please Enter Fall Time on Keyboard", standardWidth, 0 + screenOffset, defaultFont.getSize(), defaultFont.getFont());
      onScreenText.draw(fallTimeInput, standardWidth, 0 + screenOffset *2, defaultFont.getSize(), defaultFont.getFont());
      enterButton.changeText("Fall Time Correct");
      break;
    case 3:
      onScreenText.draw("Please Enter 1st part of Email Address", standardWidth, 0 + screenOffset, defaultFont.getSize(), defaultFont.getFont());
      onScreenText.draw(emailFirstInput, standardWidth, 0 + screenOffset *2, defaultFont.getSize(), defaultFont.getFont());
      enterButton.changeText("Before @ Correct");
      break;
    case 4:  
      onScreenText.draw("Please Enter 2nd part of Email Address", standardWidth, 0 + screenOffset, defaultFont.getSize(), defaultFont.getFont());
      onScreenText.draw(emailSecondInput, standardWidth, 0 + screenOffset *2, defaultFont.getSize(), defaultFont.getFont());
      enterButton.changeText("After @ Correct");
      break;
    case 5:  
      onScreenText.draw("Please Enter 3rd part of Email Address", standardWidth, 0 + screenOffset, defaultFont.getSize(), defaultFont.getFont());
      onScreenText.draw(emailThirdInput, standardWidth, 0 + screenOffset *2, defaultFont.getSize(), defaultFont.getFont());
      enterButton.changeText("After . Correct");
      break;
    }
  }
  void update()
  {
    if (backButton.checkMouse() && mousePressed)
    {
      //reset for next time
      enterButton.draw();
      enterButton.changeText("Enter Details");
      currentState = 0;
      screenCount = 0;
      initialScreen = true;
      fallTimeScreen = false;
      emailPart1Screen = false;
      emailPart2Screen = false;
      emailPart3Screen = false;
    } else
      if (enterButton.checkMouse() && screenCount == 0 && initialScreen && mousePressed)
      {//reset inputs, toggle keyboard
        fallTimeInput = "";
        emailFirstInput = "";
        emailSecondInput = "";
        emailThirdInput = "";
        emailInput = "";
        nameInput = "";
        screenCount = 1;
        initialScreen = false;
        nameScreen = true;
        KetaiKeyboard.toggle(fallDetector);
        nameDetInput = true;
        redraw();
      } else
        if (enterButton.checkMouse() && screenCount == 1 && nameScreen && mousePressed)
        {
          screenCount = 2;
          nameScreen = false;
          nameDetInput = false;
          fallTimeScreen = true;
          fallDetInput = true;
          if (nameInput == "")
          {
            nameInput = "User of fall detector who you know";//default if user doesn't enter anything
          }
        } else
          if (enterButton.checkMouse() && screenCount == 2 && fallTimeScreen && mousePressed)
          {
            fallTimeScreen = false;
            emailPart1Screen = true;
            screenCount=3;
            fallDetInput = false;
            emailDetInput = true;
            if (fallTimeInput == "")
            {
              fallTimeInput = "10";//default if user doesn't enter anything
            }
            redraw();
          } else
            if (enterButton.checkMouse() && screenCount == 3 && emailPart1Screen && mousePressed)
            {
              emailPart1Screen = false;
              emailPart2Screen = true;
              screenCount=4;
              emailDetInput = false;
              emailDet2Input = true;
              redraw();
            } else
              if (enterButton.checkMouse() && screenCount == 4 && emailPart2Screen && mousePressed)
              {
                emailDet2Input = false;
                emailDet3Input = true;
                screenCount = 5;
                emailPart2Screen = false;
                emailPart3Screen = true;
                redraw();
              } else
                if (enterButton.checkMouse() && screenCount == 5 && emailPart3Screen && mousePressed)
                {
                  KetaiKeyboard.hide(fallDetector);
                  emailDet3Input = false;
                  emailPart3Screen = false;
                  if (emailFirstInput == "" || emailSecondInput == "" || emailThirdInput == "")//if nothing is entered for email
                  {
                    loadEmail();//load the email address
                    formatEmail();//format the email address
                    appendStrings(emailFirstInput, emailSecondInput, emailThirdInput);//append it to single email address
                  }
                  enterButton.hide();
                  redraw();
                  formatData();//formal single strings into arrays
                  saveData();//save in external text file
                  fallTimeInt = setupState.stringToInt(fallTimeInput);
                  screenCount = 0;
                }
  }
  void clearScreen()
  {
    background(appColour[0], appColour[1], appColour[2]);
  }
  void loadData()
  {
    //loads data into arrays
    name = loadStrings("userName.txt");
    fallTime = loadStrings("userFallTime.txt");
    email = loadStrings("userEmailAddress.txt");
    email2 = loadStrings("userEmail2Address.txt");
    email3 = loadStrings("userEmail3Address.txt");
  }
  //changes string[] to single string
  void makeDataSingleString()
  {
    nameInput = join(name, "");
    emailFirstInput = join(email, "");
    emailSecondInput = join(email2, "");
    emailThirdInput = join(email3, "");
    fallTimeInput = join(fallTime, "");
  }
  //changes string to int
  int stringToInt(String a)
  {
    int loadedInt =  Integer.parseInt(a);
    return loadedInt;
  }
  void formatData()
  {
    name = split(nameInput, ' ');
    fallTime = split(fallTimeInput, ' ');
    email = split(emailFirstInput, ' ');
    email2 = split(emailSecondInput, ' ');
    email3 = split(emailThirdInput, ' ');
  }
  //saves string[] to txt file
  void saveData()
  {
    saveStrings("name.txt", name);
    saveStrings("userFallTime.txt", fallTime);
    saveStrings("userEmailAddress.txt", email);
    saveStrings("userEmail2Address,txt", email2);
    saveStrings("userEmail3Address.txt", email3);
  }
  void loadEmail()
  {
    email = loadStrings("userEmailAddress.txt");
    email2 = loadStrings("userEmail2Address.txt");
    email3 = loadStrings("userEmail3Address.txt");
  }
  void formatEmail()
  {
    emailFirstInput = join(email, "");
    emailSecondInput = join(email2, "");
    emailThirdInput = join(email3, "");
  }
}
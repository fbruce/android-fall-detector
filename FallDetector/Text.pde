class Text
{
  String textInput;
  int posX, posY, size, strokeWeight;
  PFont defaultFont;
 void draw(String textInput, int posX, int posY, int size, PFont font)
 {
   this.textInput = textInput;
   this.posX = posX;
   this.posY = posY;
   this.size = size;
   this.defaultFont = font;
   textFont(font,size);
   text(textInput, posX, posY);
 }
 void setStrokeWeight(int strokeWeight)
 {
   this.strokeWeight = strokeWeight;
   strokeWeight(strokeWeight);
 }

}
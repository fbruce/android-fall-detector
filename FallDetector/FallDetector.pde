import cassette.audiofiles.*;
import cassette.audiofiles.SoundFile;
import ketai.camera.*;
import ketai.data.*;
import ketai.net.*;
import ketai.sensors.*;
import ketai.ui.*;
import com.temboo.core.*;
import com.temboo.Library.Google.Gmail.*;

KetaiSensor sensor;
FallDetectState fallState = new FallDetectState();
InitialState startState = new InitialState();
InstructionState instructionState = new InstructionState();
SetupState setupState = new SetupState();
StateHandler handleState = new StateHandler();
Font defaultFont;// = new Font();
TembooSession session = new TembooSession("falldetectoruws", "myFirstApp", "94335426936d4ee8941b6e47ffa06b94"); // Create a session using your Temboo account application details
PApplet fallDetector;//used to access context  
SoundFile backgroundMusic, alarmMusic;

int[] appColour = {144, 137, 137};//standard background colour for application
float rotationX, rotationY, rotationZ;//used with ketai for detecting falls
float accelerometerX, accelerometerY, accelerometerZ;//used with ketai to detect falls
float linX, linY, linZ;//used with ketai to detect falls
int screenOffset = 0;
int currentState = 0;//used to change states
boolean validFallDetected = false;
boolean displayKeyboard = false;

//due to setup of ketai library these need to be public.
//keypress must be carried out in main class otherwise not detected. 
boolean fallDetected = false, emailDetInput = false, fallDetInput = false, emailDet2Input = false, nameDetInput = false, emailDet3Input = false;
String emailFirstInput = "";
String fallTimeInput = "";
String emailSecondInput = "";
String emailInput = "";
String nameInput = "";
String emailThirdInput = "";
String[] fallTime = new String[1];
String[] email = new String[1];
String[] email2= new String[1];
String[] email3 = new String[1];
String[] name = new String[1];
int[] intFallTime;
int fallTimeInt = 0;
Text onScreenText;

//used with ketai library for fall detection
float finalOutput, yOutput, xOutput, zOutput, verticalAcceleration;
int standardWidth = 0;

void setup()
{
  size(displayWidth, displayHeight, P3D);
  screenOffset = displayHeight/10;//used for placement of text
  orientation(PORTRAIT);
  frameRate(30);
  textAlign(CENTER);
  imageMode(CENTER); 
  standardWidth = displayWidth/2;
  //nameInput = "Fiona";
  //fallTimeInput = "22";
  // fallTimeInt = 22;
  // emailInput = ("lalala@hawwd.com");
  background(appColour[0], appColour[1], appColour[2]);
  sensor = new KetaiSensor(this);
  sensor.start();
  //defaultFont.init();
  defaultFont = new Font();
  setupState.init();
  fallState.init();
  alarmMusic = new SoundFile(this, "alarm.mp3");
  fallDetector = this;
  onScreenText = new Text();
}
void draw()
{
  handleState.draw();
  detectFall();
}
void mousePressed()
{
  handleState.mousePressed();
}
void onGyroscopeEvent(float x, float y, float z)
{
  rotationX = x;
  rotationY = y;
  rotationZ = z;
}
void onAccelerometerEvent(float x, float y, float z)
{
  accelerometerX = x;
  accelerometerY = y;
  accelerometerZ = z;
}
void onLinearAccelerationEvent(float x, float y, float z)
{
  linX = x;
  linY = y;
  linZ = z;
}
void detectFall()
{

  finalOutput = sqrt((accelerometerX * accelerometerX) + (accelerometerY * accelerometerY) + (accelerometerZ * accelerometerZ));
  finalOutput -= 9.9;
  verticalAcceleration = abs(xOutput * sin(rotationZ)) + (yOutput * sin(rotationY)) - (zOutput * cos(rotationY) * cos(rotationZ));

  if (currentState == 3)
  {
    if (linY >= 4.0)
    {
      validFallDetected = true;
      fallState.checkFall();
    } 
  }
}
void keyPressed()
{
  if (nameDetInput)
  {
    if (keyCode == 67)
    {
      if (nameInput.length() > 0) 
      {
        nameInput = nameInput.substring(0, nameInput.length()-1);
      }
    } else 
    if (keyCode >= 29 && keyCode <= 54)
    {
      nameInput = nameInput + key;
    } else
    {
      onScreenText.draw("Please Enter Valid Input", standardWidth, 0 + (screenOffset * 3), defaultFont.getSize(), defaultFont.getFont());
    }
  }

  if (fallDetInput)
  {
    if (keyCode == 67)
    {
      if (fallTimeInput.length() > 0) 
      {
        fallTimeInput = fallTimeInput.substring(0, fallTimeInput.length()-1);
      }
    }
    else 
    if (keyCode >= 7 && keyCode <= 16)
    {
      if (fallTimeInput.length() < 3)
      {
        fallTimeInput = fallTimeInput + key;
      } 
      else
      {
        onScreenText.draw("Too many numbers", standardWidth, 0 + (screenOffset * 3), defaultFont.getSize(), defaultFont.getFont());
      }
    }
    else if (keyCode < 7 || keyCode >16)
    {
      onScreenText.draw("Please only enter numbers", standardWidth, 0 + (screenOffset * 3), defaultFont.getSize(), defaultFont.getFont());
    }
  } 

  if (emailDetInput)
  {
    if (keyCode == 67)
    {
      if (emailFirstInput.length() > 0) 
      {
        emailFirstInput = emailFirstInput.substring(0, emailFirstInput.length()-1);
      }
    }else if ((keyCode >= 29 && keyCode <= 54) || (keyCode >= 7 && keyCode <= 16))
    {
      emailFirstInput = emailFirstInput + key;
    } else
    {
      onScreenText.draw("Please enter valid input", standardWidth, 0 + (screenOffset * 3), defaultFont.getSize(), defaultFont.getFont());
    }
  }
  if (emailDet2Input)
  {
    if (keyCode == 67)
    {
      if (emailSecondInput.length() > 0) 
      {
        emailSecondInput = emailSecondInput.substring(0, emailSecondInput.length()-1);
      }
    } else if ((keyCode >= 29 && keyCode <= 54) || (keyCode >= 7 && keyCode <= 16))
      {
        emailSecondInput = emailSecondInput + key;
      } else
      {
        onScreenText.draw("Please enter valid input", standardWidth, 0 + (screenOffset * 3), defaultFont.getSize(), defaultFont.getFont());
      }
  }
  if (emailDet3Input)
  {
    if (keyCode == 67)
    {
      if (emailThirdInput.length() > 0) 
      {
        emailThirdInput = emailThirdInput.substring(0, emailThirdInput.length()-1);
      }
    } else if ((keyCode >= 29 && keyCode <= 54))
      {
        emailThirdInput = emailThirdInput + key;
      } else
      {
        onScreenText.draw("Please enter valid input", standardWidth, 0 + (screenOffset * 3), defaultFont.getSize(), defaultFont.getFont());
      }
  }
}
//adds start and end of email address together with an @ symbol in the middle. 
String appendStrings(String a, String b, String c)
{
  emailInput = a + "@" + b + "." + c; 
  return emailInput;
}
class StateHandler
{
  StateHandler()
  {
  }
  void draw()
  {
    switch(currentState)
    {
    case 0: 
      startState.draw();
      break;
    case 1: 
      instructionState.draw();
      break;
    case 2:
      setupState.draw();
      break;
    case 3: 
      fallState.draw();
      break;
    default : 
      background(0);
      break;
    }
  }
  void mousePressed()
  {
    switch(currentState)
    {
    case 0: 
      startState.update();
      break;
    case 1: 
      instructionState.update();
      break;
    case 2:
      setupState.update();
      appendStrings(emailFirstInput, emailSecondInput, emailThirdInput);
      break;
    case 3: 
      fallState.update();
      break;
    default : 
      background(0);
      break;
    }
  }
}
void clearScreen()
{
  background(0, 0, 0);
}
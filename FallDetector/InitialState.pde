class InitialState implements State
{//sets buttons in initial state
  Button instructionButton, setupButton, fallDetectButton;
 InitialState()
 {
    instructionButton = new Button("Instructions" , 0.0);
    setupButton = new Button( "Setup Application" , 1.0);
    fallDetectButton = new Button("Detect Falls" , 2.0);
 }
 void draw()
 {
   clearScreen();
   instructionButton.draw();
   setupButton.draw();
   fallDetectButton.draw(); 
 }  
 void init()
 {
   instructionButton.init();
   setupButton.init();
   fallDetectButton.init();
 }
 void update()
 {
   if (instructionButton.checkMouse() && mousePressed)
   {
     currentState = 1;
   }
   if(setupButton.checkMouse() && mousePressed)
   {
     currentState = 2;
   }
   if(fallDetectButton.checkMouse() && mousePressed)
   {
     currentState = 3;
   }
 }
 void clearScreen()
{
  background(appColour[0], appColour[1], appColour[2]);
}
}
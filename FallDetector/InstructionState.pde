class InstructionState implements State
{
  Button backButton;
  Text onScreenText;
  InstructionState()
  {
    backButton = new Button("Go Back", 2.0);
    onScreenText = new Text();
  }
  void init()
  {
    backButton.init();
  }
  void draw()
  {//draws instructions on screen
    clearScreen();
    backButton.draw();  
    onScreenText.draw("Application Instructions", standardWidth, 0 + screenOffset, defaultFont.getSize(), defaultFont.getFont());
    onScreenText.draw("Enables Phone to Detect Falls", standardWidth, 0  + (screenOffset *2), defaultFont.getSize(), defaultFont.getFont());
    onScreenText.draw("User sets up contact details", standardWidth, 0 + (screenOffset *3), defaultFont.getSize(), defaultFont.getFont());
    onScreenText.draw("If Fall is not cancelled", standardWidth, 0  + (screenOffset*4), defaultFont.getSize(), defaultFont.getFont());
    onScreenText.draw("Third party is contacted", standardWidth, 0  + (screenOffset*5), defaultFont.getSize(), defaultFont.getFont());
  }
  void update()
  {
    if (backButton.checkMouse() && mousePressed)
      currentState = 0;
  }
  void clearScreen()
  {
    background(appColour[0], appColour[1], appColour[2]);
  }
}